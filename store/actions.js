export const SET_GIRLS = 'SET_GIRLS';

import Data from '../girls.json'

export function getGirls(){
  return (dispatch) => {
    setTimeout(() => {
      const payload  = Data.girls
      dispatch({type: SET_GIRLS, payload})
    }, 2000);
  }
}
