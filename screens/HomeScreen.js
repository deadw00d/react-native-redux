import * as WebBrowser from 'expo-web-browser';
import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import * as Actions from '../store/actions';
import { MonoText } from '../components/StyledText';
import { bindActionCreators } from "redux";
import { connect } from 'react-redux';
class HomeScreen extends React.Component{

  static navigationOptions()  {
    return {
      header: null,
    }
  }

  componentDidMount() {
    this.props.getGirls(); //call our action
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.contentContainer}>
          {
            this.props.girls.map( girl => (
              <MonoText style={styles.codeHighlightText} key={girl.name}>
                { girl.name }
              </MonoText>
            ))
          }
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  contentContainer: {
    paddingTop: 50,
    justifyContent: 'center',
    alignItems: 'center'
  }
});

function mapStateToProps(state, props) {
  let path = state.girlsReducer
  return {
    loading: path.loading,
    girls: path.girls
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
