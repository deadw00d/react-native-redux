import { combineReducers } from 'redux';

import { SET_GIRLS } from "./actions"

let girlsState = {
  girls: [],
  loading: true
}

const girlsReducer = (state = girlsState, action) => {
  switch (action.type) {
    case SET_GIRLS:
      return {...state, girls: action.payload, loading: false};
    default:
      return state;
  }
};

// Combine all the reducers
const rootReducer = combineReducers({
  girlsReducer
  // ,[ANOTHER REDUCER], [ANOTHER REDUCER] ....
})

export default rootReducer;
